var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config')
var db = require('./db');
var modules = require('./modules');
var app = express();
var cors = require('cors');
db();
app.use(cors());
app.use(bodyParser());

app.get('/', function (req, res) {
  res.json({valor:config.hello});
});
app.use('/api',modules);
app.get('/:id',
	config.midretorno,function(req, res){
	console.log(req);
	res.send(req.retorno);
});
var port = process.env.PORT || 3000
app.listen(port, function () {
  console.log('Example app listening on port ' + port);
});

