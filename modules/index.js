var express = require('express');
var router = express.Router();
var agendamentos = require('./agendamentos');
var atendimentos = require('./atendimentos');
var avaliacoes = require('./avaliacoes');
var locais = require('./locais');
var users = require('./users');

router.use('/agendamentos',agendamentos);
router.use('/atendimentos',atendimentos);
router.use('/avaliacoes',avaliacoes);
router.use('/locais',locais);
router.use('/users',users);

module.exports = router;