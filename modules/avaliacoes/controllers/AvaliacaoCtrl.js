var Avaliacao = require('../../../models/avaliacaoModel');

module.exports = {
	getAll : function(req,res){
		Avaliacao.find(function(err,avaliacoes){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar fichas de avaliação')
			}else{
				res.json({avaliacoes:avaliacoes});
			}
		})
	},getOne:function(req,res){
		Avaliacao.findOne({_id:req.params.id}).populate('cliente').exec(function(err,avaliacao){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar ficha de avaliação')
			}else{
				res.json({avaliacao:avaliacao});
			}
		})
	},create:function(req,res){
		var avaliacao = new Avaliacao(req.body);
		avaliacao.save(function(err){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao cadastrar ficha de avaliação')
			}else{
				res.json({sucesso:true});
			}
		});
	},update:function(req,res){
		Avaliacao.update({_id:req.params.id},req.query,function(err,data){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao atualizar ficha de avaliação')
			}else{
				res.json({avaliacao:avaliacao});
			}
		})
	},delete:function(req,res){
		Avaliacao.delete({_id:req.params.id},function(err,avaliacao){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao remover ficha de avaliação')
			}else{
				res.json({avaliacao:avaliacao});
			}
		})
	}
}
