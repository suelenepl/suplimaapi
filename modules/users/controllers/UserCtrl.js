var User = require('../../../models/userModel');

module.exports = {
	getAll : function(req,res){
		User.find(function(err,users){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar usuários')
			}else{
				res.json({users:users});
			}
		})
	},getOne:function(req,res){
		User.findOne({_id:req.params.id},function(err,user){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar usuário')
			}else{
				res.json({user:user});
			}
		})
	},create:function(req,res){
		var user = new User(req.body);
		user.save(function(err){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao cadastrar usuário')
			}else{
				res.json({sucesso:true});
			}
		});
	},update:function(req,res){
		delete req.query._id;
		console.log(req.query);
		req.query.updatedAt = Date.now();
		User.update({_id:req.params.id},req.query,function(err,data){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao atualizar usuário')
			}else{
				res.json({user:data});
			}
		})
	},delete:function(req,res){
		User.delete({_id:req.params.id},function(err,user){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao remover usuário')
			}else{
				res.json({user:user});
			}
		})
	}
}