var Agenda = require('../../../models/agendaModel');

module.exports = {
	getAll : function(req,res){
		Agenda.find(function(err,agendamentos){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar agendamentos')
			}else{
				res.json({agendamentos:agendamentos});
			}
		})
	},getOne:function(req,res){
		Agenda.findOne({_id:req.params.id},function(err,agendamento){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar agendamento')
			}else{
				res.json({agendamento:agendamento});
			}
		})
	},create:function(req,res){
		var agenda = new Agenda(req.body);
		agenda.save(function(err){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao cadastrar agendamento')
			}else{
				res.json({sucesso:true});
			}
		});
	},update:function(req,res){
		Agenda.update({_id:req.params.id},req.query,function(err,data){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao atualizar agendamento')
			}else{
				res.json({agendamento:agendamento});
			}
		})
	},delete:function(req,res){
		Agenda.delete({_id:req.params.id},function(err,agendamento){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao remover agendamento')
			}else{
				res.json({agendamento:agendamento});
			}
		})
	}
}
