var Atendimento = require('../../../models/atendimentoModel');

module.exports = {
	getAll : function(req,res){
		Atendimento.find(function(err,atendimentos){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar atendimentos')
			}else{
				res.json({atendimentos:atendimentos});
			}
		})
	},getOne:function(req,res){
		Atendimento.findOne({_id:req.params.id}).populate('cliente').exec(function(err,atendimento){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar atendimento')
			}else{
				res.json({atendimento:atendimento});
			}
		})
		

	},getByUser:function(req,res){
		Atendimento.find({cliente:req.params.id}).populate('cliente').exec(function(err,atendimentos){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar ficha de atendimentos')
			}else{
				res.json({atendimentos:atendimentos});
			}
	})
	},create:function(req,res){
		var atendimento = new Atendimento(req.body);
		atendimento.save(function(err){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao cadastrar atendimento')
			}else{
				res.json({sucesso:true});
			}
		});
	},update:function(req,res){
		Atendimento.update({_id:req.params.id},req.query,function(err,data){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao atualizar atendimento')
			}else{
				res.json({atendimento:data});
			}
		})
	},delete:function(req,res){
		Atendimento.delete({_id:req.params.id},function(err,atendimento){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao remover atendimento')
			}else{
				res.json({atendimento:atendimento});
			}
		})
	}
}
