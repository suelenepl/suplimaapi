var Local = require('../../../models/localModel');

module.exports = {
	getAll : function(req,res){
		Local.find(function(err,locais){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar Locais')
			}else{
				res.json({locais:locais});
			}
		})
	},getOne:function(req,res){
		Local.findOne({_id:req.params.id},function(err,local){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao consultar local')
			}else{
				res.json({local:local});
			}
		})
	},create:function(req,res){
		var local = new Local(req.body);
		local.save(function(err){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao cadastrar local')
			}else{
				res.json({sucesso:true});
			}
		});
	},update:function(req,res){
		Local.update({_id:req.params.id},req.query,function(err,data){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao atualizar local')
			}else{
				res.json({local:local});
			}
		})
	},delete:function(req,res){
		Local.delete({_id:req.params.id},function(err,local){
			if(err){
				console.log(err);
				res.status(500).send('Erro ao remover local')
			}else{
				res.json({local:local});
			}
		})
	}
}
