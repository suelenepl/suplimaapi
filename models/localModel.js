var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var localSchema = new Schema({
	tipoLocal:{tipo: String,
		descricao: String			
		},
	possuiEndereco:Boolean,
	address:{
		address1: String, //numero
		address2: String, //rua
		address3: String, //complemento
		address4: String, //bairro
		city:String,
		state:String,
		postalCode:String,
		country:String
		},
	createdAt: Date,
	updatedAt: Date,
	createdBy: {
			type:Schema.Types.ObjectId,
			ref:'User'
		}
});

module.exports = mongoose.model('Local', localSchema);