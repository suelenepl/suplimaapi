var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tratamentoSchema = new Schema({

	numSessoesPrev:Number,
	dataAltaPrev:Date,
	dataAlta:Date,
});

module.exports = mongoose.model('Tratamento', tratamentoSchema);
