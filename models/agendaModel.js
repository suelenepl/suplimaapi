var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var agendaSchema = new Schema({
	profissional:{
			type:Schema.Types.ObjectId,
			ref:'User'
		},
	cliente:{
			type:Schema.Types.ObjectId,
			ref:'User'
		},
	dataAgenda: Date,
	local:{ //local da clínica ou atendimento homecare
			type:Schema.Types.ObjectId,
			ref:'Local'
		},
	createdAt: Date,
	updatedAt: Date,
	createdBy: {
			type:Schema.Types.ObjectId,
			ref:'User'
		},
});

module.exports = mongoose.model('Agenda', agendaSchema);