var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var atendimentoSchema = new Schema({
	profissional: String,
	/*profissional:{
			type:Schema.Types.ObjectId,
			ref:'User'
	},*/
	cliente:{
			type:Schema.Types.ObjectId,
			ref:'User'
	},
	local:{ //local da clínica ou atendimento homecare
			type:Schema.Types.ObjectId,
			ref:'Local'
	}, 
	dataAtend: Date,
	peso: Number,
	finalidade:String,
	inspecao:String,
	palpacao:String,
	sinVitais:{
			pa:{//pressão arterial
				sis:Number, 
				dia: Number
				}, 
			spO2: Number, //saturação O2
			fc: Number, //frequencia cardiaca
			fr: Number, //frequencia respiratoria
			tCelsius: Number, //temperatura corporal
			au: String //ausculta do pulmão
	},
			/*força e amplitude de movimento, a força é medida de 0 a 5
			0-não tem contração muscular
			1-não vence a gravidade mas tem contração muscular
			2-vence a gravidade mas não mantém por 5s
			3-vence a gravidade mas não a resistência
			4-Não mantém por 5s
			5-mantém a contração muscular com resistência por 5s*/
	medicao:{ 
	  	membro:String, //ex braço esquerdo
	  	movimento:String, //ou manobra/técnica
	  	forca:Number,
	  	adm:Number //em graus do goniometro
	},
	evolucao:String,
	createdAt: {type:Date,default:Date.now},
	updatedAt: Date,
	createdBy: {
			type:Schema.Types.ObjectId,
			ref:'User'
	}
});

module.exports = mongoose.model('Atendimento', atendimentoSchema);
