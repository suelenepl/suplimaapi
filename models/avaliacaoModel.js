var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var avaliacaoSchema = new Schema({
	qp:String, //queixa principal
	diagnostico:{ //diagnóstivo médico e fisioterapeutico
			clin:{
				hda:{//histórico da doença pregressa
					cid:String,
					doenca:String,
					hist:String,
					sintom:String
				}, 
			},
	}, 
	ap:{  //antecedentes pessoais
		has: Boolean, //hipertensao arterial
		diabetes:{
			possui:Boolean, 
			tipo:String, //Tipo 1 ou 2
			obs:String
		},
		tireoide:{
			problema:Boolean, 
			tipo:String, //Hipertireoidismo ou Hipotireoidismo ou outro
			obs:String
		}, 
		cirurgia:{
			possui:Boolean, 
			tipo:String, 
			obs:String
		}, 
		ca: { //cancer
			possui:Boolean, 
			tipo:String,
			obs:String
		},
		tabagismo:{ 
			possui:Boolean,
			qtdMacosDia: Number,
			obs:String
		},
		etilismo:{
			possui:Boolean,
			frq:String, //moderadamente, socialmente e consumo intenso
			obs:String
		},
		atvFisica:{
			possui:Boolean,
			frq:String,
			obs:String
		},
		alergia:{
			possui: Boolean,
			tipo:String,
			obs:String
		},
		outros:String
	},
	af:String,
	/*{ //antecedentes familiares
		parente:{
			has: Boolean, //hipertensao arterial
			diabetes:{
				possui:Boolean, 
				tipo:String, //Tipo 1 ou 2
				obs:String
			},
			ca:{ //cancer
				possui:Boolean, 
				tipo:String,
				obs:String
			},
			outros:String,
		}
	},*/ 
	medicamentos:String, //medicamentos exames
	examesRecentes:String,
	pesoInicial:Number,
	altura:Number,
	profissional:{
			type:Schema.Types.ObjectId,
			ref:'User'
	},
	cliente:{
			type:Schema.Types.ObjectId,
			ref:'User'
	},
	createdAt: {type:Date,default:Date.now},
	updatedAt: Date,
	createdBy: {
			type:Schema.Types.ObjectId,
			ref:'User'
	}
});

module.exports = mongoose.model('Avaliacao', avaliacaoSchema);