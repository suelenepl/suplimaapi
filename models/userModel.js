var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	userName:String,
	password:String,
	name:String,
	lastName:String,
	role:String,
	active:Boolean,
	phones:String,
	emails:String,
	profissional:{
		crefito:String,
		especialidade:String,
		},
	doc:{
		cpf:String,
		rgRne:String,
		dataExp:Date,
		orgEmissor:String,
		birth:{
			date:Date,
			city:String,
			state:String,
			country:String,
			pai:String,
			mae:String,
			},
		},
	profession:String,
	escolaridade:String,
	address:{
		address1: String, //numero
		address2: String, //rua
		address3: String, //complemento
		address4: String, //bairro
		city:String,
		state:String,
		postalCode:String,
		country:String
		},
	createdAt: {type:Date,default:Date.now},
	updatedAt: Date,
	createdBy: {
			type:Schema.Types.ObjectId,
			ref:'User'
		}
}, {
  toObject: {
  virtuals: true
  },
  toJSON: {
  virtuals: true 
  }
});

userSchema.virtual('age').get(function(age){
return moment().diff(this.doc.birth.date, 'years')
})

userSchema.virtual('fullName').get(function(fullName){
	if(this.name){
		return this.name + ' ' + this.lastName;
	}
	return '';

})

module.exports = mongoose.model('User', userSchema);

