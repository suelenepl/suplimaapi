var mongoose = require('mongoose');
var config = require('../config');

module.exports = function(){
	var mongoUrl = process.env.MONGODB_URI || config.dbUrl;
	mongoose.connect(mongoUrl,function(err,res){
		if(err){
			console.log(err);
		}
	})

};